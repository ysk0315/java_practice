package stringMethod;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//問題1
		String alh = "As Leading Hamonaizer";
		String start = "As";
		String end = "zer";

		if(alh.startsWith(start)) {
			System.out.println(alh + "は" + start + "から始まっている");
		}
		if(alh.endsWith(end)) {
			System.out.println(alh + "は" + end + "で終わっている");
		}

		//問題2
		String greeting = "私の名前はALH太郎です";
		String part = greeting.substring(2, 10);
		System.out.println(part);

		//問題3
		String department = "CEH, WEB, AMU, ROU, GAU, FAU";
		String[] departments = department.split(",");

		for(String dep : departments) {
			System.out.println(dep.trim());
		}

		//問題4
		String numberOnly = "1231445145113123";
		String regex = "^[0-9].+$";

		if(numberOnly.matches(regex)) {
			System.out.println("「" + numberOnly + "」は数字だけで構成されています");
		}
	}

}
