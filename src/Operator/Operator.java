package operator;

public class Operator {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("演算子");
		System.out.println("【問題1】");
		int num = 3;
		int result = 0;

		//加算
		result = 6 + num;
		System.out.println(6 + "+" + num + "=" + result);

		//減算
		result = 6 - num;
		System.out.println(6 + "-" + num + "=" + result);

		//乗算
		result = 6 * num;
		System.out.println(6 + "*" + num + "=" + result);

		//除算
		result = 6 / num;
		System.out.println(6 + "/" + num + "=" + result);

		//剰余
		result = 6 % num;
		System.out.println(6 + "%" + num + "=" + result);

		System.out.println("【問題2】");
		int num2 = 3;
		int incrementResult = 0;
		int decrementResult = 0;

		//1加算
		incrementResult = num2++;
		System.out.println("3を１加算した数は" + incrementResult);

		num2 = 3;
		//1減算
		decrementResult = num--;
		System.out.println("3を１減算した数は" + decrementResult);

		System.out.println("【問題3】");
		int num3 = 13;
		int div3 = 5;
		int result3 = 0;

		//商
		result3 = num3 / div3;
		System.out.println(num3 + "を" + div3 + "で割った商は" + result3);
		//余り
		result3 = num3 % div3;
		System.out.println(num3 + "を" + div3 + "で割った余りは" + result3);
	}

}
