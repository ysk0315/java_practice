package exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		File file = new File("sample.txt");
		try {
			printLength(null);

			String str = "あいうえお";
			int i = Integer.parseInt(str);

			int[] numbers = {1, 2, 3, 4, 5};
			int array = numbers[5];

			FileReader fr = new FileReader(file);
		} catch(FileNotFoundException e) {
			System.out.println("ファイルが存在しません。");
			System.out.println(e);
		} catch(NumberFormatException e) {
			System.out.println(e);
		} catch(NullPointerException e) {
			System.out.println(e);
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		}
	}

	static void printLength(String str) {
		int i = str.length();
		System.out.println(str + "は" + i + "文字");
	}

}
