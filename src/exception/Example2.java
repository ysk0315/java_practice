package exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Example2 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		File file = new File("sample.txt");
		if(!(file.exists())) {
			System.out.println("ファイルの存在が確認できなかったので、returnで処理を終了します。");
			return;
		}

		try {
			FileReader fr = new FileReader(file);
		} catch(FileNotFoundException e) {
			System.out.println("ファイルが存在しません.。");
			System.out.println(e);
		}
	}

}
