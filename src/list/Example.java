package list;

import java.util.ArrayList;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//問題1
		ArrayList<Integer> values = new ArrayList<Integer>();
		ArrayList<String> alphabet = new ArrayList<String>();

		values.add(50);
		values.add(55);
		values.add(70);
		values.add(65);
		values.add(80);

		alphabet.add("A");
		alphabet.add("B");
		alphabet.add("C");
		alphabet.add("D");
		alphabet.add("E");

		for(int i = 0; i < values.size(); i++) {
			System.out.println(values.get(i));
		}
		for(int i = 0; i < alphabet.size(); i++) {
			System.out.println(alphabet.get(i));
		}

		//問題2
		ArrayList<Integer> valuesList = new ArrayList<Integer>();

		valuesList.add(50);
		valuesList.add(55);
		valuesList.add(70);
		valuesList.add(65);
		valuesList.add(80);

		valuesList.set(2, 0);

		for(int i = 0; i < valuesList.size(); i++) {
			System.out.println(valuesList.get(i));
		}
			//奇数番目を0で置き換える
		for(int i = 0; i < valuesList.size(); i++) {
			if(i % 2 == 0) {
				valuesList.set(i, 0);
			}
			System.out.println(valuesList.get(i));
		}

		//問題3
		ArrayList<Integer> value = new ArrayList<Integer>();

		value.add(50);
		value.add(55);
		value.add(70);
		value.add(65);
		value.add(80);

		int multiple = value.get(1) * 2;
		value.set(1, multiple);

		for(int i = 0; i < value.size(); i++) {
			System.out.println(i + 1 + "番目の要素：" + value.get(i));
		}
		for(int i = 0; i < value.size(); i++) {
			if(i % 2 != 0) {
				value.set(i, value.get(i) * 3);
			}
			System.out.println(i + 1 + "番目の要素：" + value.get(i));
		}
	}

}
