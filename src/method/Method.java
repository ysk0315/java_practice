package method;

public class Method {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//問題1～2
		countDown();
		countDown(10);
		countUp();
		countUp(10);

		//問題3
		int front = 10;
		int back = 3;

		int product = multiplication(front, back);
		System.out.println(front + "*" + back + "=" + product);
		int product2 = division(front, back);
		System.out.println(front + "/" + back + "=" + product2);

		//問題4
		int x = 10;
		int y = 20;
		int z = 30;

		System.out.println(maxValue(x, maxValue(y, z)));
		System.out.println(minValue(x, minValue(y, z)));
	}

	//問題1～2
	private static void countDown() {
		System.out.println("カウントダウン");
		for(int i = 5; i >= 0; i--) {
			System.out.println(i);
		}
	}
	private static void countDown(int start) {
		System.out.println("カウントダウン" + start + "開始");
		for(int i = start; i >= 0; i--) {
			System.out.println(i);
		}
	}
	private static void countUp() {
		System.out.println("カウントアップ");
		for(int i = 0; i <= 5; i++) {
			System.out.println(i);
		}
	}
	private static void countUp(int end) {
		System.out.println("カウントアップ" + end + "終了");
		for(int i = 0; i <= end; i++) {
			System.out.println(i);
		}
	}

	//問題3
	private static int multiplication(int front, int back) {
		int result = front * back;
		return result;
	}
	private static int division(int front, int back) {
		int result = front / back;
		return result;
	}

	//問題4
	static int maxValue(int n1, int n2) {
		if(n1 > n2) {
			return n1;
		} else {
			return n2;
		}
	}
	static int minValue(int n1, int n2) {
		if(n1 < n2) {
			return n1;
		} else {
			return n2;
		}
	}

}
