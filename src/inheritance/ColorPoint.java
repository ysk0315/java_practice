package inheritance;

public class ColorPoint extends Point {
	String color;

	void printInfo() {
		super.printInfo();
		System.out.println("座標値は(" + this.x + "," + this.y + ")で、色は" + this.color + "です。");
	}
}
