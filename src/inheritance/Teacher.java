package inheritance;

public class Teacher extends Person {
	void work() {
		System.out.println("教員です。授業をします。");
	}
	void makeTest() {
		System.out.println("試験問題を作ります。");
	}
}
