package inheritance;

public class Inheritance {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//Point・ColorPointクラス参照
		Point p = new Point();
		p.x = 3;
		p.y = 6;
		p.printInfo();

		ColorPoint cp = new ColorPoint();
		cp.x = 5;
		cp.y = 10;
		cp.color = "赤";
		cp.printInfo();


		//Person・Student・Teacherクラス参照
		Person[] persons = new Person[3];
		persons[0] = new Person();
		persons[1] = new Student();
		persons[2] = new Teacher();

		for(int i = 0; i < persons.length; i++) {
			if(persons[i] instanceof Person) {
				System.out.println("persons[" + i + "]はPersonクラスのインスタンスです");
			}
			if(persons[i] instanceof Student) {
				System.out.println("persons[" + i + "]はStudentクラスのインスタンスです");
			}
			if(persons[i] instanceof Teacher) {
				System.out.println("persons[" + i + "]はTeacherクラスのインスタンスです");
			}
			persons[i].work();
		}


		//Carクラス参照
		Car c = new Car();
		c.speedUp();


	}

}
