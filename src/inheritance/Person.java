package inheritance;

public class Person {
	void work() {
		System.out.println("人です。仕事します。");
	}

	void workThreeTimes(Person p) {
		p.work();
		p.work();
		p.work();
	}
}
