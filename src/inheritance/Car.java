package inheritance;

public class Car {
	private int speed;

	public void speedUp() {
		while(speed < 80) {
			speed++;
			System.out.println("スピードアップ！");
			if(this.speed == 30 || this.speed == 60) {
				System.out.println("まだまだスピードアップ！");
			}
		}
	}

	public void speedDown() {
		if(speed > 0) {
			speed--;
		}
	}
}
