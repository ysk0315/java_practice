package array;

public class Array {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("【問題1】");
		int[] values = {50, 55, 70, 65, 80};

		System.out.println("全要素の値");
		for(int i = 0; i < values.length; i++) {
			System.out.println("values[" + i + "]=" + values[i] );
		}
		System.out.println("要素数は、" + values.length + "です。");
		for(int j = 0; j < 3; j++) {
			System.out.println("values[" + j + "]=" + values[j] );
		}

		System.out.println("【問題2】");
		int[] num = {1, 9, 93, 19, 11, 76, 9, 66, 44, 33};
		int maxValue = 0;

		for(int k = 0; k < num.length - 1; k++) {
			if(num[k] > num[k+1]) {
				maxValue = num[k];
			} else {
				maxValue = num[k+1];
			}
			if(num[num.length - 1] > maxValue) {
				maxValue = num[num.length - 1];
			}
		}
		System.out.println("最大値=" + maxValue);

		System.out.println("【問題3】");
		int a = 7;

		for(int c = 1; b < 10; b++) {
			int result = a * c;
			System.out.print(result + " ");
		}
	}

}