package fileReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {
	public static void main(String[] args) {
		File file = new File("C:\\Users\\okada.yoshiki\\Desktop", "sample.txt");
		System.out.println(file.getName());
		BufferedReader br = null;

		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}

		} catch(IOException e) {
			System.out.println("例外が発生しました");
			System.out.println(e);
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println(e);
				}
			}
		}
	}
}
