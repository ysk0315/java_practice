package logicalOperator;

public class LogicalOperator {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("【問題1】");
		int small1 = 4;
		int middle1 = 10;
		int medium1 = 10;
		int large1 = 12;

		if((small1 < middle1) && (middle1 < large1)) {
			System.out.println("middleはsmallよりも大きい かつ largeはmiddleよりも大きい");
		}
		if((middle1 != medium1) || (large1 > small1)) {
			System.out.println("middleとmediumは等しくない または largeはsmallよりも大きい");
		}
		if((middle1 > small1 * 2) && (large1 == small1 * 3)) {
			System.out.println("middleはsmallの2倍以上大きく、largeはsmallの3倍と等しい");
		}

		System.out.println("【問題2】");
		int year2;

		year2 = 2016;
		if((year2 % 4 == 0) || (year2 % 2 == 0)) {
			System.out.println("夏季もしくは冬季のオリンピック開催年です。");
		} else if((year2 % 4 != 0) && (year2 % 2 != 0)) {
			System.out.println("オリンピック開催年ではありません。");
		}

		System.out.println("【問題3】");
		int num3;

		num3 = 15;
		if((num3 % 3 == 0) && (num3 % 5 == 0)) {
			System.out.println("FizzBuzz");
		} else if(num3 % 3 == 0) {
			System.out.println("Fizz");
		} else if(num3 % 5 == 0) {
			System.out.println("Buzz");
		}

		System.out.println("【問題4】");
		int midTermScore3 = 80;
		int FinalScore3 = 60;

		if((midTermScore3 >= 60 && FinalScore3 >= 60) ||
				(midTermScore3 + FinalScore3 >= 130) ||
				((midTermScore3 + FinalScore3 >= 100) && ((midTermScore3 >= 90) || (FinalScore3 >= 90)))) {
			System.out.println("合格");
		} else {
			System.out.println("不合格");
		}
	}

}
