package conditionalBranch2;

public class ConditionalBranch2 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("【問題1】");
		int sum = 0;
		for(int i = 1; i < 101; i++) {
			if(i % 2 == 0) {
				sum += i;
			}
		}
		System.out.println(sum);

		System.out.println("【問題2】");
		int[] score = {0, 1, 2, 1, 0, 3, 0, 1, 0, 1, 0, 1, 0, 0, 2, 1, 3, 0};

		int giants = 0;
		int tigers = 0;

		for(int j = 1; j < 10; j++) {
			giants += score[j * 2 -2];
			tigers += score[j * 2 - 1];
		}

		if(giants < tigers) {
			System.out.println(tigers + "対"  + giants + "で、タイガースの勝ちです！");
		} else if(giants > tigers) {
			System.out.println(tigers + "対"  + giants + "で、ジャイアンツの勝ちです！");
		} else {
			System.out.println("引き分けです");
		}

		System.out.println("【問題3】");
		int value = 77;

		if((value % 1 == 0) && (value % value == 0)) {
			System.out.println(value + "は、素数です。");
		}

		System.out.println("【問題4】");
		int[] num = {1, 9, 93, 19, 11, 76, 9, 66, 44, 33};

		int maxValue = 0;
		int minValue = 0;
		minValue = 1;

		for(int k = 0; k < num.length; k++) {
			if(num[k] > maxValue) {
				maxValue = num[k];
			}
			if(num[k] < minValue) {
				minValue = num[k];
			}

		}

		System.out.println("最大値=" + maxValue + "最小値=" + minValue);


		System.out.println("【問題5】");
		int[] values = {32, 23, 56, 12, 63, 33, 28, 91, 75, 43, 51};

		for(int m = 0; m < values.length; m++) {
			for(int n = m + 1; n < values.length; n++) {
				if(values[m] > values[n]) {
					int max = values[m];
					int min = values[n];
					values[m] = min;
					values[n] = max;
				}
			}
		}
		for(int m = 0; m < values.length; m++) {
			System.out.println(values[m]);
		}
	}

}
