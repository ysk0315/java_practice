package inheritance2;

public class CustomerManager {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		CustomerCard[] card = new CustomerCard[100];
		card[0] = new ShoeShopCustomerCard("山田太郎", "東京都", 26.5);
		card[1] = new ShoeShopCustomerCard("佐藤花子", "神奈川県", 24.5);
		card[2] = new HatShopCustomerCard("鈴木健司", "茨城県", 26.0);
		card[3] = new HatShopCustomerCard("渡辺進", "東京都", 57.0);

		for(int i = 0; i < 100; i++) {
			if(card[i] == null) {
				break;
			}
			System.out.println(i + "番目の顧客カードに記載の情報");
			card[i].printInfo();
			System.out.println("===============================");
		}
	}

}
