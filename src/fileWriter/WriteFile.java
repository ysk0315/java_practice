package fileWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class WriteFile {
	public static void main(String[] args) {
		File file = new File("C:\\Users\\okada.yoshiki\\Desktop", "sample.txt");
		ArrayList<String> branches = new ArrayList<String>();

		branches.add("目黒本社");
		branches.add("横浜事業所");
		branches.add("大阪事業所");
		branches.add("福岡事業所");

		BufferedWriter bw = null;
		PrintWriter pw = null;
		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			pw = new PrintWriter(bw);
			for(int i = 0; i < branches.size(); i++) {
				pw.println(branches.get(i));
			}

		} catch(IOException e) {
			System.out.println("例外が発生しました");
			System.out.println(e);
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("close処理中に例外が発生しました");
					System.out.println(e);
				}
			}
		}
	}
}
