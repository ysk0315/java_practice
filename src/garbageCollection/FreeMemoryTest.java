package garbageCollection;

public class FreeMemoryTest {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("空きメモリサイズ：" + Runtime.getRuntime().freeMemory());
		DataSet[] data = new DataSet[10000];
		for(int i = 0; i < 10000; i++) {
			data[i] = new DataSet();
			System.out.println("生成済みインスタンス数：" + (i + 1) + " 空きメモリサイズ：" + Runtime.getRuntime().freeMemory());
		}

	}

}

class DataSet{
	int x;
	int y;
}