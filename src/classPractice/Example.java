package classPractice;

public class Example {
	public static void main(String[] args) {
		//問題1
		Human tarao = new Human();
		tarao.nickname = "タラちゃん";
		tarao.age = 3;

		Human sazae = new Human();
		sazae.nickname = "サザエさん";
		sazae.age = 27;

		Human okada = new Human();
		okada.nickname = "よしきち";
		okada.age = 26;
		okada.height = 170;
		okada.weight = 60;

		System.out.println(tarao.nickname + "は" + tarao.age + "歳");
		System.out.println(okada.nickname + "は" + okada.age + "歳で、" + okada.height + "cmで、" + okada.weight + "kg");
        System.out.println(Human.birthAge(sazae.age, tarao.age));

        //問題2
        double edge = 3.0;
        System.out.println("1辺の長さが" + edge + "の立方体の一面当たりの面積は、" + Cube.area(edge) + "です。");
        System.out.println("1辺の長さが" + edge + "の立方体の体積は、" + Cube.volume(edge) + "です。");
	}
}

//問題1
class Human{
	String nickname;
	int age;
	double height;
	double weight;

	public static int birthAge(int motherAge, int childAge) {
		int birth = motherAge - childAge;
		return birth;
	}
}

//問題2
class Cube{
	double edge;

	public static double area(double edge) {
		double area = edge * edge;
		return area;
	}
	public static double volume(double edge) {
		double volume = edge * edge * edge;
		return volume;
	}
}