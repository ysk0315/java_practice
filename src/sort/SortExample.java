package sort;

import java.util.ArrayList;
import java.util.Collections;

public class SortExample {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		ArrayList<Point> pointList = new ArrayList<Point>();
		pointList.add(new Point(0, 8));
		pointList.add(new Point(1, 6));
		pointList.add(new Point(2, 9));
		pointList.add(new Point(3, 3));

		Collections.sort(pointList);

		for(Point p : pointList) {
			System.out.println("(" + p.x + "," + p.y + ")->" + (p.x + p.y));
		}
	}

}

class Point implements Comparable<Point>{
	int x;
	int y;

	Point(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int compareTo(Point p) {
		//↓compareToメソッドの挙動確認（どのようにして大小関係を比較しているのか）
		System.out.println("(" + this.x + "+" + this.y + ")" + "-" + "(" + p.x + "+" + p.y + ")" + "=" + ((this.x + this.y) - (p.x + p.y)));
		System.out.println("");
		//↑ここまで
		return (this.x + this.y) - (p.x + p.y);
	}
}