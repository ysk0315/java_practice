package sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputOutput {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("あなたのお名前は？");
		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader reader = new BufferedReader(in);

		try {
			String name = reader.readLine();
			System.out.println("こんにちは、" + name + "さん！");
		} catch(IOException e) {
			System.out.println(e);
		}

		System.out.println("数字を入力");
		InputStreamReader in2 = new InputStreamReader(System.in);
		BufferedReader reader2 = new BufferedReader(in2);
		try {
			String line = reader2.readLine();
			double val = Double.parseDouble(line);
			System.out.println("入力された値の平方根は" + Math.sqrt(val));
		} catch(IOException e) {
			System.out.println(e);
		}
	}

}
