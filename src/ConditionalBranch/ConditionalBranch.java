package conditionalBranch;

public class ConditionalBranch {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("【問題1】");
		int num1 = 20;

		//変数numの値が20の場合
		if(num1 == 20) {
			System.out.println("num1は20である");
		}
		//numが偶数の場合
		if(num1 % 2 == 0) {
			System.out.println("num1は偶数である");
		}
		//numを3で割った余りが2の場合
		if(num1 % 3 == 2) {
			System.out.println("num1を3で割った余りは2である");
		}

		System.out.println("【問題2】");
		int year2 = 2100;

		if(year2 % 4 == 0 && year2 % 100 == 0) {
			if(year2 % 400 != 0) {
				System.out.println(year2 + "年はうるう年ではありません。");
			} else {
				System.out.println(year2 + "年はうるう年です。");
			}
		} else {
			System.out.println(year2 + "年はうるう年ではありません。");
		}

		System.out.println("【問題3】");
		int money3 = 1000;
		int product3 = 140;
		int change3 = money3 -product3;

		System.out.println(product3 + "円の商品を" + money3 + "円で購入した場合のおつりは・・・");

		if(change3 / 100 >= 1) {
			System.out.println("100円玉は" + (change3 / 100) + "枚");
	        change3 = change3 % 100;
		}
		if(change3 / 50 >= 1) {
			System.out.println("50円玉は" + (change3 / 50) + "枚");
			change3 = change3 % 50;
		}
		if(change3 / 10 >= 1) {
			System.out.println("10円玉は" + (change3 / 10) + "枚");
			change3 = change3 % 10;
		}

		System.out.println("【問題4】");
		int currentMonth = 9;
		System.out.println("現在は" + currentMonth + "月です。");
		//条件式（31日があるか）
		switch(currentMonth){
		case 2:
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("31日ではありません。");
			break;
		default:
			System.out.println("31日まであります");
		}

		switch(currentMonth){
		case 5:
		case 6:
		case 7:
		case 8:
			System.out.println("Rがつきません。");
			break;
		default:
			System.out.println("Rがつきます。");
		}
	}

}
