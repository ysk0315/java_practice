package collection;

import java.util.LinkedList;
import java.util.Queue;

public class CollectionExample {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		Queue<String> queue = new LinkedList<String>();

		queue.offer("(1)");
		System.out.println("キューの状態：" + queue);
		queue.offer("(2)");

		System.out.println("キューの状態：" + queue);
		queue.offer("(3)");
		System.out.println("キューの状態：" + queue);
		queue.offer("(4)");
		System.out.println("キューの状態：" + queue);

		while(!queue.isEmpty()) {
			System.out.println("要素の取り出し：" + queue.poll());
			System.out.println("キューの状態；" + queue);
		}

		LinkedList<String> stack = new LinkedList<String>();

		stack.addLast("(1)");
		System.out.println("スタックの状態：" + stack);
		stack.addLast("(2)");
		System.out.println("スタックの状態：" + stack);
		stack.addLast("(3)");
		System.out.println("スタックの状態：" + stack);
		stack.addLast("(4)");
		System.out.println("スタックの状態：" + stack);

		while(!stack.isEmpty()) {
			System.out.println("要素の取り出し：" + stack.removeLast());
			System.out.println("スタックの状態：" + stack);
		}

	}

}
