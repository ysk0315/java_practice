package api;

import java.util.Random;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//Randomクラスについて
		Random rand = new Random();
		System.out.println(rand.nextDouble());

		//Stringクラスについて
		String msg1 = new String("こんにちは");
		String msg2 = new String("こんにちは");
		String msg3 = msg1;
		System.out.println(msg1 == msg2);
		System.out.println(msg1 == msg3);

		String str = "Javaの学習";
		System.out.println(str.length());
		System.out.println(str.indexOf("学習"));
		System.out.println(str.contains("Java"));
		System.out.println(str.replace("Java", "Java言語"));

		String year = "2009/11/22";
		String[] items = year.split("/");
		for(int i = 0; i < items.length; i++) {
			System.out.println(items[i]);
		}

		//Mathクラスについて
		System.out.println("-5の絶対値は" + Math.abs(-5));
		System.out.println("3.0の平方根は" + Math.sqrt(3.0));
		System.out.println("半径2の円の面積は" + 2 * 2 * Math.PI);
		System.out.println("sin60°は" + Math.sin(60.0 * Math.PI / 180.0));
	}

}
