package integerMethod;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//問題1
		int value = Integer.parseInt("100");
		int value2 = Integer.valueOf("100");

		if(value < 500) {
			System.out.println(value + "は500未満");
		} else {
			System.out.println(value + "は500以上");
		}
		if(value2 < 500) {
			System.out.println(value2 + "は500未満");
		} else {
			System.out.println(value2 + "は500以上");
		}

		//問題2
		int maxValue = Integer.max(100, 50);
		System.out.println("「100」と「50」を比較したとき、大きい数値は「" + maxValue + "」です。");
	}

}
