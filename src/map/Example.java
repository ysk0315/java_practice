package map;

import java.util.HashMap;

public class Example {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//問題1
		HashMap<String, String> branch = new HashMap<String, String>();

		branch.put("社名", "ALH株式会社");
		branch.put("福岡事業所", "福岡県福岡市");
		branch.put("横浜事業所", "神奈川県横浜市");

		System.out.println(branch.get("社名"));
		System.out.println(branch.get("福岡事業所"));
		System.out.println(branch.get("横浜事業所"));

		//問題2
		HashMap<String, String> branches = new HashMap<String, String>();
		HashMap<String, String> addresses = new HashMap<String, String>();

		branches.put("東京本社", "〒153-0063");
		branches.put("横浜事業所", "〒220-0012");
		branches.put("大阪事業所", "〒530-0051");
		branches.put("福岡事業所", "〒810-0001");

		addresses.put("〒153-0063", "東京");
		addresses.put("〒220-0012", "神奈川");
		addresses.put("〒530-0051", "大阪");
		addresses.put("〒810-0001", "福岡");

		for(String key : branches.keySet()) {
			System.out.println(key + "の郵便番号は" + branches.get(key) + "です");
		}
		for(String key : addresses.keySet()) {
			System.out.println(key + "は、" + addresses.get(key) + "です");
		}

		//問題3
		HashMap<String, Integer> test = new HashMap<String, Integer>();
		test.put("国語", 87);
		test.put("数学", 81);
		test.put("理科", 94);
		test.put("社会", 79);
		test.put("英語", 85);

		int sum = 0;

		for(String key : test.keySet()) {
			if(test.get(key) <= 90) {
				test.put(key, test.get(key) + 10);
			} else {
				test.put(key, test.get(key));
			}
		}

		for(String key : test.keySet()) {
			System.out.println(key + "の点数は" + test.get(key) + "点です。");
		}
	}

}
