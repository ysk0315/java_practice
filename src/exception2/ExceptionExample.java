package exception2;

public class ExceptionExample {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		int[] scores = new int[5];
		int a = 4;
		int b = (int)(Math.random() * 10);
		System.out.println("b=" + b);

		try {
			int c = a / b;
			System.out.println("cの値は" + c);
			scores[b] = 10;
			System.out.println("処理が正常に行われました");
		} catch(ArithmeticException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
